Simple Framework with Cucumber - Java for UI and API Testing with Selenium and Karate

	Following target is to be run for testing
		mvn clean integration-test
		
	To run any specific tags or multiple tags [note: multiple tags will 	run in parallel]
		-Dtagstorun=@myexampletest
		
	To run either UIfeature, APIfeature, APItags or all in parallel 	specify values with comma separated [either tags,features or none(mandatory)]
		-Dparallelmode=API_TAG_PARALLEL
		
	Execution modes Enum 
		API_TAG_PARALLEL("apitagsparallel"),
		API_FEATURE_PARALLEL("apifeaturesparallel"),
		API_FEATURE_SEQUENTIAL("apifeaturessequential"), 
		API_TAG_SEQUENTIAL("apitagssequential"), 
		UI_TAG_PARALLEL("uitagsparallel"), 
		UI_FEATURE_PARALLEL("uifeaturesparallel"), 
		UI_FEATURE_SEQUENTIAL("uifeaturessequential");
		
	To specify test environment details
		-Dkarate.env=myenvironment
		
	To generate report	
		-Dgeneratereport=true
		
		