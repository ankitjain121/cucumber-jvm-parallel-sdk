package com.test.sixt.sixtforce.automation.test.framework.runner;

public class TestEnv {

	public static final String SIXTFORCE_QA = "sixtforce_qa";
	public static final String SIXTFORCE_PROD = "sixtforce_prod";

	public static String getConsoleUrl(String env) {
		for (Env e : Env.values()) {
			if (e.name().equalsIgnoreCase(env)) {
				return e.consoleUrl;
			}
		}
		return null;
	}

	public static int getEnvId(String env) {
		for (Env e : Env.values()) {
			if (e.name().equalsIgnoreCase(env)) {
				return e.envId;
			}
		}
		return 0;
	}

	public static String getEnvShortName(int envId) {
		for (Env e : Env.values()) {
			if (e.envId == envId) {
				return e.name().toLowerCase();
			}
		}
		return null;
	}

	private enum Env {

		SIXTFORCE_QA(1, "http://test.salesforce.com/"),
		SIXTFORCE_PROD(2, "https://sixt.my.salesforce.com/");

		private String consoleUrl;
		private int envId;

		private Env(int envId, String consoleUrl) {
			this.consoleUrl = consoleUrl;
			this.envId = envId;
		}

	}

	public static void main(String[] args) {
		System.out.println(getConsoleUrl("ft2"));
	}

}
