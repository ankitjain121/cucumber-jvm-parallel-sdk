package com.test.sixt.sixtforce.automation.test.buddybooster.ui.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.test.sixt.sixtforce.automation.test.buddybooster.ui.constants.BuddyBooster_LoginPOConstants;
import com.test.sixt.sixtforce.automation.test.framework.runner.TestEnv;

import init.DriverManager;

public class BuddyBooster_LoginPageObject{

	WebDriver driver;
	//Assertions assertions = new Assertions();

	public BuddyBooster_LoginPageObject() {
		System.out.println("Initializing Page Objects ...");
		driver = DriverManager.getdriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		PageFactory.initElements(driver, this);
	//	assertions.initializeAssert();
	}

	/*--------------------------------------------------- HOME PANNEL --------------------------------------------------*/
	@FindBy(css = BuddyBooster_LoginPOConstants.HOMEPAGE)
	private WebElement HOMEPAGE;

	public String getCurrentURL() {
	//	SoftAssert sa = new SoftAssert();
		Boolean status = false;
		driver.get(BuddyBooster_LoginPOConstants.URL);
		status = HOMEPAGE.findElement(By.xpath(BuddyBooster_LoginPOConstants.HOMETITLE)).isDisplayed();
		String myURL = (status? "PASS" : "FAIL");
	//	assertions.assertEqual(status, "Pass");
	//	assertions.softAssertEqual(status, "Pass");
		return myURL;
	}


	public void openURL(String arg1) {
		switch (arg1) {
		case TestEnv.SIXTFORCE_QA:
			driver.get(TestEnv.getConsoleUrl(arg1));
			break;
		case TestEnv.SIXTFORCE_PROD:
			driver.get(TestEnv.getConsoleUrl(arg1));
			break;
		default:
			driver.get(TestEnv.getConsoleUrl(TestEnv.SIXTFORCE_QA));
			break;
		}
		
	}


	public boolean checkIfPageExists() {
		if((driver.getTitle()!=null && driver.getCurrentUrl()!=null)?true:false)
			return true;
		return false;
	}


	public void teardown() {
		driver.close();
		
	}
	
	
}
