package com.test.sixt.sixtforce.automation.test.buddybooster.ui.stepdefs;

import com.test.sixt.sixtforce.automation.test.buddybooster.ui.pageobjects.BuddyBooster_LoginPageObject;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BuddyBooster_Login_Steps {

	BuddyBooster_LoginPageObject poObject = new BuddyBooster_LoginPageObject();

	@Given("^i am valid user and able to launch \"([^\"]*)\"$")
	public void i_am_valid_user_and_able_to_launch(String arg1) throws Throwable {
		System.out.println("trying to open my browser with url" + arg1);
		poObject.openURL(arg1);
		throw new PendingException();
	}

	@When("^i enter \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_and(String arg1, String arg2) throws Throwable {
		System.out.println("test data" + arg1 + " >> " + arg2);
		// poObject.loginToPage(arg1, arg2);
	}
	
	@Then("^the page should open with \"([^\"]*)\"$")
	public void the_page_should_open_with(String arg1) throws Throwable {
		poObject.checkIfPageExists();
	}

	@Then("^buddy booster icon is displayed$")
	public void buddy_booster_icon_is_displayed() throws Throwable {
		System.out.println("icon displayed");
	}

	@After
	public void tearDownDriver() {
		poObject.teardown();
	}

}
