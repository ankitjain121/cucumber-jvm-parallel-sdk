@test
Feature: Login to Buddy Booster application 

Scenario: Verify Buddy Booster Login page
	Given i am valid user and able to launch "<site Url>"
    When i enter "<username>" and "<password>"
    Then the page should open with "<title>"
    Then buddy booster icon is displayed
