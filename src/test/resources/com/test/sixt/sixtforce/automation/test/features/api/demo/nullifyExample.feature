@demo
Feature: Verify that products are properly returned by the API 

Background: 
	* url baseUrl
	
	* def nullify = 
	"""
	function(o) {
	  for (var key in o) {
	    if (o[key] == '') o[key] = null;
	  }
	  return o;
	}
	"""
	
	* def verifyStatusAndResponse = 
	"""
	function(responseStatus, response, reqJson){
		var validationStatus = false;
		switch(responseStatus){
		case 200: 
			validationStatus = (response.segmentId == reqJson.segmentId && response.modifierValue == reqJson.modifierValue && response.featureValue == reqJson.featureValue && response.feature == reqJson.feature && response.dataCenter == reqJson.dataCenter && response.ttl == reqJson.ttl);
			break;
		case 500: 
			validationStatus = true;
			break;
		case 400: 
			validationStatus = true;
			break;
		}
		return validationStatus;
		}
	"""


Scenario Outline: Verify Country region for Add event in APD feature
	* def reqJson = {"segmentId": <segmentId>, "modifierValue": <modifierValue>, "featureValue": <featureValue>, "feature":<feature>, "dataCenter":<dataCenter>, "ttl":<ttl>, "isAutomatic":<isAutomatic>}
	* def reqJson = nullify(reqJson)
	Given   path '/svc/modifier' 
	And request reqJson
	* configure headers = read('classpath:headers.js')
	When method post  
	Then status <status>
	* def statusCode = verifyStatusAndResponse(responseStatus, response, reqJson)
	* match statusCode == true
	
	Examples:
	| segmentId | modifierValue | featureValue	| feature 				|	dataCenter 	 |  ttl  |	isAutomatic		|	status	|
	| 10860399  | 1000		    | "AF:JY"       | "COUNTRY_REGION"  	|	"NYM"		 |  3600 |	true			|	200		|
	| 10860399  | 1000		    | "aF:JY"       | "COUNTRY_REGION"  	|	"NYM"		 |  3600 |	true			|	500		|
	