package utils;

import java.util.Random;

public class Utils {
	
	public static final String ALPHB_VALUES = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	/**
	 * Method return the random number within the specified range.
	 * 
	 * @param range
	 * @return random value within the specified range.
	 */
	public static int randInt(int range) {
		Random rand = new Random();
		return rand.nextInt(range);

	}
	
	public static String getRandomChars(int length) {
		if(length > 255) length = 255;
		StringBuilder sb = new StringBuilder();
		int max = ALPHB_VALUES.length();
		for(int i=0;i<length;i++){
			int r = randInt(max);
			sb.append(ALPHB_VALUES.charAt(r));
		}
		return sb.toString();
	}
	
	/**
	 * Thread.sleep for timeInSeconds
	 * @param timeInSeconds
	 */
	public static void wait(int timeInSeconds) {
		try {
			Thread.sleep(timeInSeconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
