package utils;

import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class Assertions{
	private static SoftAssert sa;
	
	public static SoftAssert initializeAssert() {
		if (sa == null)
			try {
				new SoftAssert();
			} catch (Exception e) {}
		return sa;
	}

	public void assertAll() {
		sa.assertAll();
	}
	
	public void softAssertEqual(Object actual, Object expected) {
		sa.assertEquals(actual, expected);
	}
	
	public void assertEqual(Object actual, Object expected) {
		Assert.assertEquals(actual, expected);
	}

	public void softAssertEqual(Object actual, Object expected, String msg) {
		sa.assertEquals(actual, expected, msg);
	}
	
	public void assertEqual(Object actual, Object expected, String msg) {
		Assert.assertEquals(actual, expected, msg);
	}

	public void softAssertNotEqual(Object actual, Object expected) {
		sa.assertNotEquals(actual, expected);
	}
	
	public void assertNotEqual(Object actual, Object expected) {
		Assert.assertNotEquals(actual, expected);
	}

	public void softAssertNotEqual(Object actual, Object expected, String msg) {
		sa.assertNotEquals(actual, expected, msg);
	}
	
	public void assertNotEqual(Object actual, Object expected, String msg) {
		Assert.assertNotEquals(actual, expected, msg);
	}

	public void assertNull(Object value, String msg) {
		Assert.assertNull(value, msg);
	}

	public void assertNotNull(Object value, String msg) {
		Assert.assertNotNull(value, msg);
	}

	public void assertTrue(boolean value, String msg) {
		Assert.assertTrue(value, msg);
	}

	public void assertFalse(boolean value, String msg) {
		Assert.assertFalse(value, msg);
	}

	public void assertSame(Object actual, Object expected, String message) {
		Assert.assertSame(actual, expected, message);
	}

	public void assertNotSame(Object actual, Object expected, String message) {
		Assert.assertNotSame(actual, expected, message);
	}

	public void assertEqualsNoOrder(Object[] actual, Object[] expected, String message) {
		Assert.assertEqualsNoOrder(actual, expected, message);
	}

	public void fail(String msg) {
		Assert.fail(msg);
	}


}
