package init;

import java.io.File;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class DriverManager extends EventFiringWebDriver {
	public static int invocationcount = 0;

	public DriverManager() {
		super(getdriver());
	}

	public static WebDriver getdriver() {
		if (null == PropertyLoader.provider)
			PropertyLoader.init();
		invocationcount++;
		try {
			DesiredCapabilities capabilities = null;
			
			switch (Browsers.valueOf(PropertyLoader.provider.getProperty("browsername", String.class))) {
			case CHROME:
				ChromeDriverService cds = new ChromeDriverService.Builder().usingDriverExecutable(
						new File(PropertyLoader.provider.getProperty("chromebrowserdriverpath", String.class))
								.getAbsoluteFile())
						.usingAnyFreePort().build();

				return new ChromeDriver(cds);

			case FIREFOX:
				GeckoDriverService gds = new GeckoDriverService.Builder().usingDriverExecutable(
						new File(PropertyLoader.provider.getProperty("webdriver.gecko.driver", String.class))
								.getAbsoluteFile())
						.usingAnyFreePort().build();
				return new FirefoxDriver(gds);

			case INTERNET_EXPLORER:
				InternetExplorerDriverService ids = new InternetExplorerDriverService.Builder().usingDriverExecutable(
						new File(PropertyLoader.provider.getProperty("iebrowserdriverpath", String.class))
								.getAbsoluteFile())
						.usingAnyFreePort().build();
				return new InternetExplorerDriver(ids);
				
			case REMOTECHROME:
				capabilities = DesiredCapabilities.chrome();
				capabilities.setJavascriptEnabled(true);
				return new RemoteWebDriver(new URL(PropertyLoader.provider.getProperty("remotedriverurl", String.class)), capabilities);
				
			case REMOTEFIREFOX:
				capabilities = DesiredCapabilities.firefox();
				capabilities.setJavascriptEnabled(true);
				return new RemoteWebDriver(new URL(PropertyLoader.provider.getProperty("remotedriverurl", String.class)), capabilities);

			default:
				return new FirefoxDriver();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
