

package core.handlers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;

import ch.qos.logback.classic.Logger;
// TODO: Auto-generated Javadoc

/**
 * The Class FileHandler.
 */
public class FileHandler {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = null;
    
    /** The sep reg. */
    private final String sepReg = "(\\\\|/)";
    
    /** The create if not exist. */
    private boolean createIfNotExist;
    
    /** The append to file. */
    private boolean appendToFile;
    
    /** The file is writable. */
    private boolean fileIsWritable = false;
    
    /** The file is readable. */
    private boolean fileIsReadable = false;
    
    /** The file path. */
    private String filePath;
    
    /** The file name. */
    private String fileName;
    
    /** The file extension. */
    private String fileExtension;
    
    /** The writeable file. */
    private FileWriter writeableFile;
    
    /** The current file. */
    private File currentFile;
    
    /** The writable file output stream. */
    private OutputStream writableFileOutputStream;

    /**
     * Instantiates a new file handler.
     *
     * @param fileObject the file object
     * @throws Exception the exception
     */
    public FileHandler(File fileObject) throws Exception {
        if (fileObject.exists()) {
            if (fileObject.canRead()) {
                this.currentFile = fileObject;
                this.fileIsReadable = true;
            } else {
               // LOGGER.error("Unable to read '{}'", this.filePath + this.fileName);
                throw new IOException("Unable to read file " + this.filePath + this.fileName);
            }
        }
        setAbsoluteFilename(fileObject.getAbsolutePath());
    }

    
    /**
     * Instantiates a new file handler.
     *
     * @param absoluteFilename the absolute filename
     */
    public FileHandler(String absoluteFilename) {
        initialiseFile(absoluteFilename, false);
    }

   
    /**
     * Instantiates a new file handler.
     *
     * @param absoluteFilename the absolute filename
     * @param value the value
     */
    public FileHandler(String absoluteFilename, boolean value) {
        initialiseFile(absoluteFilename, value);
    }

    
    /**
     * Initialise file.
     *
     * @param absoluteFilename the absolute filename
     * @param value the value
     */
    public void initialiseFile(String absoluteFilename, boolean value) {
        setAbsoluteFilename(absoluteFilename);
        setCreateIfNotExist(value);
        setAppendToFile(false);
    }

    
    /**
     * Sets the absolute filename.
     *
     * @param value the new absolute filename
     */
    public final void setAbsoluteFilename(String value) {
        setFileName(value.replaceFirst("^.*" + sepReg, ""));
        setFilePath(value.substring(0, value.length() - this.fileName.length()));
    }

    
    /**
     * Sets the file name.
     *
     * @param value the new file name
     */
    public final void setFileName(String value) {
        if (value.matches(sepReg)) {
           // LOGGER.error("The filename '{}' is not valid!", value);
            return;
        }
        this.fileName = value;
        String[] fileComponents = this.fileName.split("\\.");
        if (fileComponents.length > 1) {
            this.fileExtension = fileComponents[fileComponents.length - 1];
        } else {
            this.fileExtension = "";
        }
    }

    
    /**
     * Gets the file name.
     *
     * @return the file name
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * Gets the extension.
     *
     * @return the extension
     */
    public String getExtension() {
        return this.fileExtension;
    }

    /**
     * Checks if is file writeable.
     *
     * @return true, if is file writeable
     */
    private boolean isFileWriteable() {
        return this.fileIsWritable;
    }

    /**
     * Sets the file path.
     *
     * @param value the new file path
     */
    public final void setFilePath(String value) {
        String[] pathExploded = value.split(sepReg);
        String path = "";
        for (String pathSegment : pathExploded) {
            path += pathSegment + System.getProperty("file.separator");
        }
        this.filePath = path;
    }

    /**
     * Gets the file path.
     *
     * @return the file path
     */
    public String getFilePath() {
        return this.filePath;
    }

    /**
     * Gets the absolute file.
     *
     * @return the absolute file
     */
    public String getAbsoluteFile() {
        return this.filePath + this.fileName;
    }

    /**
     * Sets the creates the if not exist.
     *
     * @param value the new creates the if not exist
     */
    public final void setCreateIfNotExist(boolean value) {
        this.createIfNotExist = value;
    }

    
    /**
     * Gets the creates the if not exist.
     *
     * @return the creates the if not exist
     */
    public boolean getCreateIfNotExist() {
        return this.createIfNotExist;
    }

    /**
     * Sets the append to file.
     *
     * @param value the new append to file
     */
    public final void setAppendToFile(boolean value) {
        this.appendToFile = value;
    }

    /**
     * Gets the append to file.
     *
     * @return the append to file
     */
    public boolean getAppendToFile() {
        return this.appendToFile;
    }

    /**
     * Gets the writeable file.
     *
     * @return the writeable file
     * @throws Exception the exception
     */
    public FileWriter getWriteableFile() throws Exception {
        if (!this.fileIsWritable) {
            this.openFileForWriting();
        }
        return this.writeableFile;
    }

    /**
     * Gets the writable file output stream.
     *
     * @return the writable file output stream
     * @throws Exception the exception
     */
    public OutputStream getWritableFileOutputStream() throws Exception {
        if (!this.fileIsWritable) {
            this.openFileForWriting();
        }
        return this.writableFileOutputStream;
    }

    /**
     * Gets the file.
     *
     * @return the file
     * @throws Exception the exception
     */
    public File getFile() throws Exception {
        if (!this.fileIsReadable) {
            this.openFile();
        }
        return this.currentFile;
    }

    /**
     * Open file.
     *
     * @throws Exception the exception
     */
    private void openFile() throws Exception {
        File fileToOpen = new File(this.filePath + this.fileName);
        if (fileToOpen.exists()) {
            if (fileToOpen.canRead()) {
                this.currentFile = fileToOpen;
                this.fileIsReadable = true;
            } else {
              //  LOGGER.error("Unable to read '{}'", this.filePath + this.fileName);
                throw new IOException("Unable to read file " + this.filePath + this.fileName);
            }
        } else if (this.createIfNotExist) {
            File directory = new File(this.filePath);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            fileToOpen.createNewFile();
            this.currentFile = fileToOpen;
        } else {
           // LOGGER.error("'{}' does not exist!", this.filePath + this.fileName);
            throw new IOException(this.filePath + this.fileName + "does not exist!");
        }

    }

    /**
     * Open file for writing.
     *
     * @throws Exception the exception
     */
    private void openFileForWriting() throws Exception {
        if (this.fileIsReadable != true) {
            this.openFile();
        }
        if (this.fileIsWritable == false) {
            this.currentFile.setWritable(true);
            this.fileIsWritable = true;
        }
        this.writeableFile = new FileWriter(this.currentFile, this.appendToFile);
        this.writableFileOutputStream = new FileOutputStream(this.currentFile);
        this.fileIsWritable = true;
    }

    /**
     * Write.
     *
     * @param value the value
     * @throws Exception the exception
     */
    public void write(String value) throws Exception {
        if (!this.fileIsWritable) {
            this.openFileForWriting();
        }
        this.writeableFile.write(value);
    }

    /**
     * Close.
     *
     * @throws Exception the exception
     */
    public void close() throws Exception {
        if (this.writeableFile != null) {
            this.writeableFile.close();
        }
        if (writableFileOutputStream != null) {
            this.writableFileOutputStream.close();
            this.writeableFile = null;
        }
        this.fileIsWritable = false;
        this.currentFile = null;
        this.fileIsReadable = false;
    }

    /**
     * Copy the file to a specific location.
     *
     * @param absoluteFileName - Target location for copy.
     * @return true, if successful
     * @throws Exception the exception
     */
    public boolean copyFileTo(String absoluteFileName) throws Exception {
        if (this.fileIsReadable != true) {
            this.openFile();
        }
        File fileDestination = new File(absoluteFileName);
        if (this.currentFile.exists()) {
            if (this.currentFile.canRead()) {
                try {
                    FileUtils.copyFile(this.currentFile, fileDestination);
                    return true;
                } catch (Exception Ex) {
                   // LOGGER.warn("Failed to copy file to '{}'", absoluteFileName);
                    return false;
                }
            } else {
               // LOGGER.error("Unable to read '{}'", this.filePath + this.fileName);
                throw new IOException("Unable to read file " + this.filePath + this.fileName);
            }
        } else {
            //LOGGER.error("'{}' does not exist!", this.filePath + this.fileName);
            throw new IOException(this.filePath + this.fileName + "does not exist!");
        }
    }
}
