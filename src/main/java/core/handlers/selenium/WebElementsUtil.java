package core.handlers.selenium;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import init.DriverManager;
import utils.Stopwatch;
import utils.Utils;

public class WebElementsUtil {
	private static final Logger LOG = LoggerFactory.getLogger(WebElementsUtil.class);
	public final int DEFAULT_WAIT = 30;
	private static final int DEFAULT_MAX_WAIT_ELEMENT_TOHIDE = 120;

	private WebDriver driver;
	private Actions actions;
	private JavascriptExecutor js;

	public WebElementsUtil() {
		driver = DriverManager.getdriver();
		actions = new Actions(driver);
		js = (JavascriptExecutor) driver;
	}

	public WebDriver getThisDriver() {
		return this.driver;
	}

	public Actions getActionsInstance() {
		return actions;
	}

	/**
	 * return element if it is visible on page.
	 * 
	 * @param t
	 * @param locatorValue
	 * @param wait
	 * @return
	 */
	public WebElement getVisibleElement(LocatorType t, String locatorValue, int wait) {
		WebElement element = null;

		switch (t) {
		case ID:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfElementLocated(By.id(locatorValue)));
			break;
		case CLASSNAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfElementLocated(By.className(locatorValue)));
			break;

		case XPATH:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locatorValue)));
			break;
		case CSSSELECTOR:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorValue)));
			break;

		case NAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfElementLocated(By.name(locatorValue)));
			break;

		case TAGNAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(locatorValue)));
			break;

		case LINKTEXT:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(locatorValue)));
			break;

		case PARTIALLINKTEXT:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(locatorValue)));
			break;
		default:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfElementLocated(By.id(locatorValue)));
			break;
		}

		return element;
	}

	/**
	 * return list of web element if it element identified by locator is visible on
	 * page.
	 * 
	 * @param t
	 * @param locatorValue
	 * @param wait
	 * @return
	 */
	public List<WebElement> getVisibleElements(LocatorType t, String locatorValue, int wait) {
		List<WebElement> element = null;

		switch (t) {
		case ID:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id(locatorValue)));
			break;
		case CLASSNAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className(locatorValue)));
			break;

		case XPATH:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locatorValue)));
			break;
		case CSSSELECTOR:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(locatorValue)));
			break;

		case NAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name(locatorValue)));
			break;

		case TAGNAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.tagName(locatorValue)));
			break;

		case LINKTEXT:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.linkText(locatorValue)));
			break;

		case PARTIALLINKTEXT:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.partialLinkText(locatorValue)));
			break;
		default:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id(locatorValue)));
			break;
		}

		return element;
	}

	public boolean waitUntilWebElementHidden(LocatorType t, String locatorValue, int wait) {

		boolean isHidden = false;

		switch (t) {
		case ID:
			isHidden = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.id(locatorValue)));
			break;
		case CLASSNAME:
			isHidden = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.className(locatorValue)));
			break;

		case XPATH:
			isHidden = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locatorValue)));
			break;
		case CSSSELECTOR:
			isHidden = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(locatorValue)));
			break;

		case NAME:
			isHidden = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.name(locatorValue)));
			break;

		case TAGNAME:
			isHidden = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.tagName(locatorValue)));
			break;

		case LINKTEXT:
			isHidden = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText(locatorValue)));
			break;

		case PARTIALLINKTEXT:
			isHidden = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.partialLinkText(locatorValue)));
			break;
		default:
			isHidden = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.invisibilityOfElementLocated(By.id(locatorValue)));
			break;
		}

		return isHidden;
	}

	/**
	 * return web element if it is present in DOM, this method doesn't check the
	 * visibility of <br>
	 * web element on page. For web element visibility refer
	 * {@link #getVisibleElement(LocatorType, String, int)}
	 *
	 * @param t
	 * @param locatorValue
	 * @param wait
	 * @return
	 * @see #getVisibleElement(LocatorType, String, int)
	 * 
	 */
	private WebElement getWebElement(LocatorType t, String locatorValue, int wait) {
		WebElement element = null;

		switch (t) {
		case ID:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfElementLocated(By.id(locatorValue)));
			break;
		case CLASSNAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfElementLocated(By.className(locatorValue)));
			break;

		case XPATH:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locatorValue)));

			break;
		case CSSSELECTOR:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locatorValue)));

			break;

		case NAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfElementLocated(By.name(locatorValue)));

			break;

		case TAGNAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfElementLocated(By.tagName(locatorValue)));
			break;

		case LINKTEXT:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfElementLocated(By.linkText(locatorValue)));
			break;

		case PARTIALLINKTEXT:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText(locatorValue)));

			break;
			
		default:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfElementLocated(By.id(locatorValue)));
			break;
		}

		return element;
	}

	/**
	 * return List of web element if it is present in DOM, this method doesn't check
	 * the visibility of <br>
	 * web element on page. For web element visibility refer
	 * {@link #getVisibleElements(LocatorType, String, int)}
	 *
	 * @param t
	 * @param locatorValue
	 * @param wait
	 * @return
	 */
	private List<WebElement> getWebElements(LocatorType t, String locatorValue, int wait) {
		List<WebElement> element = null;

		switch (t) {
		case ID:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id(locatorValue)));
			break;
		case CLASSNAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className(locatorValue)));
			break;

		case XPATH:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(locatorValue)));
			break;
		case CSSSELECTOR:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(locatorValue)));
			break;

		case NAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name(locatorValue)));
			break;

		case TAGNAME:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.tagName(locatorValue)));
			break;

		case LINKTEXT:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.linkText(locatorValue)));
			break;

		case PARTIALLINKTEXT:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.partialLinkText(locatorValue)));
			break;
			
		default:
			element = (new WebDriverWait(driver, wait))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id(locatorValue)));
			break;
		}

		return element;
	}

	/**
	 * return web element if it is present in DOM, this method doesn't check the
	 * visibility of <br>
	 * web element on page. For web element visibility refer
	 * {@link #getVisibleElement(LocatorType, String, int)}
	 *
	 * @param type         Locator Type
	 * @param locatorValue text value of locator.
	 * @param wait         time in sec.
	 * @throws Exception
	 */

	public WebElement getElement(LocatorType type, String locatorValue, int wait) {
		WebElement element = null;

		try {
			if (type == null)
				return null;

			element = getWebElement(type, locatorValue, wait);

			if (element == null) {
				System.out.println("got null element. retrying to find element..");
				element = getWebElement(type, locatorValue, wait);
			}

			if (element == null) {
				throw new NullPointerException(
						"Null. Couldn't find element with " + type.get() + " value :" + locatorValue + ".");
			}

		} catch (NoSuchElementException e) {

		} catch (Exception e) {
			throw e;
		}
		return element;
	}

	/**
	 * return List of web element if it is present in DOM, this method doesn't check
	 * the visibility of <br>
	 * web element on page. For web element visibility refer
	 * {@link #getVisibleElements(LocatorType, String, int)}
	 *
	 * 
	 * @param type
	 * @param locatorValue
	 * @param wait
	 * @return
	 */
	public List<WebElement> getElements(LocatorType type, String locatorValue, int wait) {
		List<WebElement> element = null;

		try {
			if (type == null)
				return null;

			element = getWebElements(type, locatorValue, wait);

			if (element == null) {
				System.out.println("got null element. retrying to find element..");
				element = getWebElements(type, locatorValue, wait);
			}

			if (element == null) {
				throw new NullPointerException(
						"Null. Couldn't find element with " + type.get() + " value :" + locatorValue + ".");
			}

		} catch (NoSuchElementException e) {

		} catch (Exception e) {
			throw e;
		}
		return element;
	}

	/**
	 * It will turn off the implicit wait.
	 * 
	 * @param driver
	 */
	public void turnOffImplicitWaits(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	/**
	 * It will set the implicit wait to 30 sec. Use only when you are using
	 * {@code turnOffImplicitWaits} method.
	 * 
	 * @param driver
	 */
	public void turnOnImplicitWaits(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT, TimeUnit.SECONDS);
	}

	/**
	 * find element with no implicit wait.
	 * 
	 * @param t
	 * @param locatorValue
	 * @return
	 */
	public WebElement getElementNoImplicitWait(LocatorType t, String locatorValue) {
		turnOffImplicitWaits(driver);
		WebElement element = getElementNoCheck(t, locatorValue);
		turnOnImplicitWaits(driver);
		return element;
	}

	/**
	 * find elements with no implicit wait.
	 * 
	 * @param t
	 * @param locatorValue
	 * @return
	 */
	public List<WebElement> getElementsNoImplicitWait(LocatorType t, String locatorValue) {
		turnOffImplicitWaits(driver);
		List<WebElement> elements = getElementsNoChecks(t, locatorValue);
		turnOnImplicitWaits(driver);
		return elements;
	}

	/**
	 * return WebElement if it is present in DOM otherwise null.
	 * 
	 * @param t
	 * @param locatorValue
	 * @return
	 */
	public WebElement isElementLocated(LocatorType t, String locatorValue) {
		try {
			return getElementNoCheck(t, locatorValue);
		} catch (NoSuchElementException e) {
			return null;
		}
	}


	/**
	 * default findElement
	 * 
	 * @param t
	 * @param locatorValue
	 * @return
	 */
	public WebElement getElementNoCheck(LocatorType t, String locatorValue) {
		turnOffImplicitWaits(driver);
		WebElement element = null;
		try {
			switch (t) {
			case ID:
				element = driver.findElement(By.id(locatorValue));
				break;
			case CLASSNAME:
				element = driver.findElement(By.className(locatorValue));
				break;

			case XPATH:
				element = driver.findElement(By.xpath(locatorValue));
				break;
			case CSSSELECTOR:
				element = driver.findElement(By.cssSelector(locatorValue));
				break;

			case NAME:
				element = driver.findElement(By.name(locatorValue));
				break;

			case TAGNAME:
				element = driver.findElement(By.tagName(locatorValue));
				break;

			case LINKTEXT:
				element = driver.findElement(By.linkText(locatorValue));
				break;

			case PARTIALLINKTEXT:
				element = driver.findElement(By.partialLinkText(locatorValue));
				break;
			default:
				element = driver.findElement(By.id(locatorValue));
				break;
			}
		} catch (Exception e) {
			// element not found .. silently ignoring
			LOG.debug("Element nocheck exception: " + locatorValue);
		}
		turnOnImplicitWaits(driver);

		return element;
	}

	/**
	 * default findElements
	 * 
	 * @param t
	 * @param locatorValue
	 * @param wait
	 * @return
	 */
	public List<WebElement> getElementsNoChecks(LocatorType t, String locatorValue) {
		List<WebElement> element = null;

		switch (t) {
		case ID:
			element = driver.findElements(By.id(locatorValue));
			break;
		case CLASSNAME:
			element = driver.findElements(By.className(locatorValue));
			break;

		case XPATH:
			element = driver.findElements(By.xpath(locatorValue));
			break;
		case CSSSELECTOR:
			element = driver.findElements(By.cssSelector(locatorValue));
			break;

		case NAME:
			element = driver.findElements(By.name(locatorValue));
			break;

		case TAGNAME:
			element = driver.findElements(By.tagName(locatorValue));
			break;

		case LINKTEXT:
			element = driver.findElements(By.linkText(locatorValue));
			break;

		case PARTIALLINKTEXT:
			element = driver.findElements(By.partialLinkText(locatorValue));
			break;
		default:
			element = driver.findElements(By.id(locatorValue));
			break;
		}

		return element;
	}

	/**
	 * {@link #getElement(LocatorType, String, int)} with {@link #DEFAULT_WAIT} =
	 * {@value #DEFAULT_WAIT}
	 * 
	 * @param locator
	 * @param locatorValue
	 * @return
	 */
	public WebElement getElement(LocatorType locator, String locatorValue) {
		return getElement(locator, locatorValue, DEFAULT_WAIT);
	}

	public WebElement getElementById(String locatorValue) {
		return getElement(LocatorType.ID, locatorValue, DEFAULT_WAIT);
	}

	public WebElement getElementByCSS(String locatorValue) {
		return getElement(LocatorType.CSSSELECTOR, locatorValue, DEFAULT_WAIT);
	}

	public WebElement getElementByXpath(String locatorValue) {
		return getElement(LocatorType.XPATH, locatorValue, DEFAULT_WAIT);
	}

	/**
	 * {@link #getElements(LocatorType, String, int)} with {@link #DEFAULT_WAIT} =
	 * {@value #DEFAULT_WAIT}
	 * 
	 * @param locator
	 * @param locatorValue
	 * @return
	 */
	public List<WebElement> getElements(LocatorType locator, String locatorValue) {
		return getElements(locator, locatorValue, DEFAULT_WAIT);
	}

	public List<WebElement> getElementsById(String locatorValue) {
		return getElements(LocatorType.ID, locatorValue, DEFAULT_WAIT);
	}

	public List<WebElement> getElementsByCSS(String locatorValue) {
		return getElements(LocatorType.CSSSELECTOR, locatorValue, DEFAULT_WAIT);
	}

	public List<WebElement> getElementsByXpath(String locatorValue) {
		return getElements(LocatorType.XPATH, locatorValue, DEFAULT_WAIT);
	}

	/**
	 * return the text of web element.
	 * 
	 * @see #getElement(LocatorType, String, int)
	 * 
	 * @param locatorType
	 * @param locatorValue
	 * @param waitInSec
	 * @return
	 */
	public String getText(LocatorType locatorType, String locatorValue, int waitInSec) {
		try {
			return getElement(locatorType, locatorValue, waitInSec).getText();
		} catch (Exception e) {
			LOG.error(String.format("Error in getting text from %s element of type %s", locatorValue,
					locatorType.toString()), e);
		}
		return "";
	}

	public String getTextById(String locatorValue) {
		return getText(LocatorType.ID, locatorValue, DEFAULT_WAIT);
	}

	public String getTextByCSS(String locatorValue) {
		return getElement(LocatorType.CSSSELECTOR, locatorValue, DEFAULT_WAIT).getText();
	}

	public String getTextByXpath(String locatorValue) {
		return getElement(LocatorType.XPATH, locatorValue, DEFAULT_WAIT).getText();
	}

	/**
	 * click web element.
	 * 
	 * @see #getElement(LocatorType, String, int)
	 * 
	 * @param locatorType
	 * @param locatorValue
	 * @param waitInSec
	 * @return
	 */
	public void clickElement(LocatorType locatorType, String locatorValue, int waitInSec) {
		getElement(locatorType, locatorValue, waitInSec).click();
	}

	/**
	 * click web element with default wait.
	 * 
	 * @see #getElement(LocatorType, String, int)
	 * 
	 * @param locatorType
	 * @param locatorValue
	 * @return
	 */
	public void clickElement(LocatorType locatorType, String locatorValue) {
		WebElement element = getElement(locatorType, locatorValue, DEFAULT_WAIT);
		clickElement(element);
	}

	public void clickElementById(String locatorValue) {
		WebElement element = getElement(LocatorType.ID, locatorValue, DEFAULT_WAIT);
		clickElement(element);
	}

	public void clickElementByCSS(String locatorValue) {
		WebElement element = getElement(LocatorType.CSSSELECTOR, locatorValue, DEFAULT_WAIT);
		clickElement(element);
	}

	public void clickElementByXpath(String locatorValue) {
		WebElement element = getElement(LocatorType.XPATH, locatorValue, DEFAULT_WAIT);
		clickElement(element);
	}

	/**
	 * wait element to be clickable before clicking element.
	 * 
	 * @param element
	 */
	public void clickElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT);
		WebElement clickable = wait.until(ExpectedConditions.elementToBeClickable(element));
		clickable.click();
	}

	public void clickCheckBoxById(String elementId) {
		String css = "#" + elementId + " +ins";
		WebElement element = getElement(LocatorType.CSSSELECTOR, css, DEFAULT_WAIT);
		scrollWebElementIntoView(element);
		element.click();
	}

	public void clickRadioById(String elementId) {
		String css = "#" + elementId + " +ins";
		WebElement element = getElement(LocatorType.CSSSELECTOR, css, DEFAULT_WAIT);
		scrollWebElementIntoView(element);
		element.click();
	}

	public void clickCheckBoxByCSS(String css) {
		css = css + " +ins";
		WebElement element = getElement(LocatorType.CSSSELECTOR, css, DEFAULT_WAIT);
		scrollWebElementIntoView(element);
		element.click();
	}

	public void clickRadioByCSS(String css) {
		css = css + " +ins";
		WebElement element = getElement(LocatorType.CSSSELECTOR, css, DEFAULT_WAIT);
		scrollWebElementIntoView(element);
		element.click();
	}

	/**
	 * send keys to web element.
	 * 
	 * @see #getElement(LocatorType, String, int)
	 * 
	 * @param locatorType
	 * @param locatorValue
	 * @param waitInSec
	 * @return
	 */
	public void sendKeys(LocatorType locatorType, String locatorValue, String text, int waitInSec) {
		WebElement element = getElement(locatorType, locatorValue, waitInSec);
		element.clear();
		element.sendKeys(text);
	}

	public void sendKeys(LocatorType locatorType, String locatorValue, String text) {
		WebElement element = getElement(locatorType, locatorValue, DEFAULT_WAIT);
		element.clear();
		element.sendKeys(text);
	}

	public void sendKeysById(String locatorValue, String text) {
		WebElement element = getElement(LocatorType.ID, locatorValue, DEFAULT_WAIT);
		element.clear();
		element.sendKeys(text);
	}

	// start dropdown related functions.

	/**
	 * select dropdown by visible text.
	 * 
	 * @param type
	 * @param locatorValue
	 * @param wait
	 * @param text
	 */
	public void selectDropDownByText(LocatorType type, String locatorValue, int wait, String text) {
		WebElement el = getElement(type, locatorValue, wait);
		if (el != null) {
			Select dropdown = new Select(el);
			dropdown.selectByVisibleText(text);
		}
	}

	/**
	 * select dropdown by visible text with default wait
	 * 
	 * @param type
	 * @param locatorValue
	 * @param text
	 */
	public void selectDropDownByText(LocatorType type, String locatorValue, String text) {
		WebElement el = getElement(type, locatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);
			dropdown.selectByVisibleText(text);
		}
	}

	/**
	 * get dropdown element by ID and select by visible text with default wait
	 * 
	 * @param type
	 * @param locatorValue
	 * @param text
	 */
	public void selectDropDownByTextID(String locatorValue, String text) {
		WebElement el = getElement(LocatorType.ID, locatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);
			dropdown.selectByVisibleText(text);
		}
	}

	/**
	 * select dropdown by value.
	 * 
	 * @param type
	 * @param locatorValue
	 * @param wait
	 * @param text
	 */
	public void selectDropDownByValue(LocatorType type, String locatorValue, int wait, String text) {
		WebElement el = getElement(type, locatorValue, wait);
		if (el != null) {
			Select dropdown = new Select(el);
			dropdown.selectByValue(text);
		}
	}

	/**
	 * select dropdown by value with default wait
	 * 
	 * @param type
	 * @param locatorValue
	 * @param text
	 */
	public void selectDropDownByValue(LocatorType type, String locatorValue, String text) {
		WebElement el = getElement(type, locatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);
			dropdown.selectByValue(text);
		}
	}

	/**
	 * get dropdown element by ID and select dropdown value by value attribute with
	 * default wait
	 * 
	 * @param type
	 * @param locatorValue
	 * @param text
	 */
	public void selectDropDownByValueID(String locatorValue, String text) {
		WebElement el = getElement(LocatorType.ID, locatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);
			dropdown.selectByValue(text);
		}
	}

	/**
	 * This Api automatically suffix <code><b>string:</b> </code> to value of
	 * dropdown options as in new implementation of ui, it is going<br>
	 * to be case for every dropdown.<br>
	 * get dropdown element by ID and select dropdown value by value attribute with
	 * default wait
	 * 
	 * @param type
	 * @param locatorValue
	 * @param text
	 */
	public void selectDropDownByStringValueID(String locatorValue, String value) {
		String modvalue = "string:" + value;
		selectDropDownByValueID(locatorValue, modvalue);
	}

	/**
	 * This Api automatically suffix <code><b>number:</b> </code> to value of
	 * dropdown options as in new implementation of ui, it is going<br>
	 * to be case for every dropdown.<br>
	 * get dropdown element by ID and select dropdown value by value attribute with
	 * default wait
	 * 
	 * @param type
	 * @param locatorValue
	 * @param text
	 */
	public void selectDropDownByNumValueID(String locatorValue, String value) {
		String modvalue = "number:" + value;
		selectDropDownByValueID(locatorValue, modvalue);
	}

	public List<String> getSelectOptions(LocatorType locatorType, String selectLocatorValue) {
		WebElement el = getElement(locatorType, selectLocatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);
			return dropdown.getOptions().parallelStream().map(opt -> opt.getText().trim()).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	/**
	 * get all element text of a dropdown select by ID of select element
	 * 
	 * @param selectLocatorValue
	 * @return List of element text
	 */
	public List<String> getSelectOptionsById(String selectLocatorValue) {
		return getSelectOptions(LocatorType.ID, selectLocatorValue);
	}

	/**
	 * get all element text of a dropdown select by CSS of select element
	 * 
	 * @param selectLocatorValue
	 * @return List of element text
	 */
	public List<String> getSelectOptionsByCss(String selectLocatorValue) {
		return getSelectOptions(LocatorType.CSSSELECTOR, selectLocatorValue);
	}

	/**
	 * get all element text of a dropdown select by XPATH of select element
	 * 
	 * @param selectLocatorValue
	 * @return List of element text
	 */
	public List<String> getSelectOptionsByXpath(String selectLocatorValue) {
		return getSelectOptions(LocatorType.XPATH, selectLocatorValue);
	}

	public List<String> getSelectOptionsValue(LocatorType locatorType, String selectLocatorValue) {
		WebElement el = getElement(locatorType, selectLocatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);
			return dropdown.getOptions().stream().map(opt -> opt.getAttribute("value")).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	/**
	 * get all element value attribute of a dropdown select by ID of select element
	 * 
	 * @param selectLocatorValue
	 * @return List of element text
	 */
	public List<String> getSelectOptionsValueById(String selectLocatorValue) {
		return getSelectOptionsValue(LocatorType.ID, selectLocatorValue);
	}

	/**
	 * get all element value attribute of a dropdown select by CSS of select element
	 * 
	 * @param selectLocatorValue
	 * @return List of element text
	 */
	public List<String> getSelectOptionsValueByCss(String selectLocatorValue) {
		return getSelectOptionsValue(LocatorType.CSSSELECTOR, selectLocatorValue);
	}

	/**
	 * get all element value attribute of a dropdown select by XPATH of select
	 * element
	 * 
	 * @param selectLocatorValue
	 * @return List of element text
	 */
	public List<String> getSelectOptionsValueByXpath(String selectLocatorValue) {
		return getSelectOptionsValue(LocatorType.XPATH, selectLocatorValue);
	}

	/**
	 * multi select elements from dropdown enabled with 'multiple' option
	 * 
	 * @param type
	 * @param locatorValue
	 * @param valueToSelect
	 */
	public void multiSelectDropDownByValue(LocatorType type, String locatorValue, List<String> valueToSelect) {
		WebElement el = getElement(type, locatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);
			el.sendKeys(Keys.LEFT_CONTROL);
			for (String value : valueToSelect)
				dropdown.selectByValue(value);
		}
	}

	/**
	 * multi select elements from dropdown enabled with 'multiple' option By
	 * deselecting the given index in multi select drop down
	 * 
	 * @param type
	 * @param locatorValue
	 * @param valueToSelect
	 * @param indexToDeselect
	 */
	public void multiSelectDropDownByValueForDefaultSelectOption(LocatorType type, String locatorValue,
			List<String> valueToSelect, int indexToDeselect) {
		WebElement el = getElement(type, locatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);

			for (String value : valueToSelect)
				dropdown.selectByValue(value);
			dropdown.deselectByIndex(indexToDeselect);
		}
	}

	/**
	 * get list of selected option in multi select dropdown.
	 * 
	 * @param locatorType
	 * @param locatorValue
	 * @return
	 */
	public List<String> getDropDownSelectedOptions(LocatorType locatorType, String locatorValue) {
		WebElement el = getElement(locatorType, locatorValue, DEFAULT_WAIT);
		List<String> selectedOptions = new ArrayList<String>();
		if (el != null) {
			Select dropdown = new Select(el);
			for (WebElement sopt : dropdown.getAllSelectedOptions()) {
				selectedOptions.add(sopt.getText());
			}
		}
		return selectedOptions;
	}

	/**
	 * get selected option in dropdown.
	 * 
	 * @param locatorType
	 * @param locatorValue
	 * @return
	 */
	public String getDropDownSelectedOption(LocatorType locatorType, String locatorValue) {
		WebElement el = getElement(locatorType, locatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);
			return dropdown.getFirstSelectedOption().getText();
		}
		return "";
	}

	public List<String> getDropDownSelectedOptionsById(String locatorValue) {
		return getDropDownSelectedOptions(LocatorType.ID, locatorValue);
	}
	// end dropdown related functions.

	public String getTextFieldAndAreaValue(LocatorType locatorType, String locatorValue) {
		WebElement tf = getElement(locatorType, locatorValue);
		return tf.getAttribute("value");
	}

	/**
	 * Use this only for angular element. This will wait upto 2 min with poll
	 * interval of 5 sec to web element(that has class attribute 'ng-hide') to be
	 * hidden.
	 * 
	 * @param maskWrapper web element that contains class ng-hide or ng-show
	 */
	public void waitAngularElementToHidden(LocatorType type, String locatorValue) {
		waitAngularElementToHidden(type, locatorValue, DEFAULT_MAX_WAIT_ELEMENT_TOHIDE);
	}

	/**
	 * Use this only for angular element. This will wait upto 2 min with poll
	 * interval of 5 sec to web element(that has class attribute 'ng-hide') to be
	 * hidden.
	 * 
	 * @param maskWrapper web element that contains class ng-hide or ng-show
	 * @param waitInSec   approx wait in sec
	 */
	public void waitAngularElementToHidden(LocatorType type, String locatorValue, int waitInSec) {
		Stopwatch st = new Stopwatch();
		turnOffImplicitWaits(driver);
		WebElement maskWrapper = getElement(type, locatorValue);
		int waitInterval = 2;
		int MAXCOUNT = waitInSec / waitInterval;
		int count = 0;
		String cssclass = maskWrapper.getAttribute("class");
		System.out.println("0: " + cssclass);
		while (!cssclass.contains("ng-hide")) {
			if (count > MAXCOUNT) {
				break;
			}
			Utils.wait(waitInterval);
			cssclass = maskWrapper.getAttribute("class");
			System.out.println(cssclass);
			count++;
		}
		turnOffImplicitWaits(driver);
		System.out.println("time taken to load page: " + st.elapsedTime());
	}


	/**
	 * This will wait upto 2 min with poll interval of 5 sec to web element to be
	 * hidden.
	 * 
	 * @param element web element that contains style attribute 'display:none'
	 */
	public void waitElementToHiddenByDisplayAttrib(LocatorType type, String locatorValue) {
		waitElementToHiddenByDisplayAttrib(type, locatorValue, DEFAULT_MAX_WAIT_ELEMENT_TOHIDE);
	}

	/**
	 * This will wait upto 2 min with poll interval of 5 sec to web element(that has
	 * style attribute 'display:none') to be hidden.
	 * 
	 * @param element   web element that contains style attribute 'display:none'
	 * @param waitInSec approx wait in sec
	 */
	public void waitElementToHiddenByDisplayAttrib(LocatorType type, String locatorValue, int waitInSec) {
		WebElement element = getElement(type, locatorValue);
		int waitInterval = 5;
		int MAXCOUNT = waitInSec / waitInterval;
		int count = 0;
		String style = element.getAttribute("style");
		style = style.replaceAll(" ", "");
		while (!style.contains("display:none;")) {
			if (count > MAXCOUNT) {
				break;
			}
			Utils.wait(waitInterval);
			style = element.getAttribute("style");
			style = style.replaceAll(" ", "");
			System.out.println(style);
			count++;
		}
	}

	/**
	 * scroll the element if it is scrollable, on current page. element should be
	 * visible.
	 * 
	 * @param id element id attribute value to be scrolled.
	 * @param h  integer value.
	 */
	public void scrollTop(String id, int h) {

		String command = "$('#" + id + "').scrollTop(" + h + ");";
		js.executeScript(command);
	}

	/**
	 * scroll to the top of the current page.
	 * 
	 */
	public void scrollTop() {

		String command = "$(window.scrollTo(0,0))";
		js.executeScript(command);
	}

	/**
	 * execute the javascript on current browser page and return the result.
	 * 
	 * @param script javascript to be executed.
	 * @return
	 */
	public Object executeJScript(String script) {
		return js.executeScript(script);
	}

	/**
	 * return the List of text contained in the web elements.
	 * 
	 * @param webelements List of web elements
	 * @return
	 */
	public List<String> webElementsToText(List<WebElement> webelements) {
		List<String> elements = new ArrayList<String>();
		for (WebElement e : webelements) {
			elements.add(e.getText().trim());
		}
		return elements;
	}

	public void clickByIdJS(String id) {

		String command = "$('#" + id + "').click();";
		js.executeScript(command);
	}

	public void clickByCSSJS(String css) {

		String command = "$('" + css + "').click();";
		js.executeScript(command);
	}

	public void clickByClassNameJS(String clsName) {

		String command = "$('." + clsName + "').click();";
		js.executeScript(command);
	}

	public void scrollToElement(String containerId, String targetElementId) {
		String js = "$('#" + containerId + "').mCustomScrollbar('scrollTo',$('#" + targetElementId + "'));";
		executeJScript(js);
	}

	public void scrollToElementByCSS(String containerCSS, String targetElementCSS) {
		String js = "$('" + containerCSS + "').mCustomScrollbar('scrollTo',$('" + targetElementCSS + "'));";
		executeJScript(js);
	}

	public void scrollTo(String containerId, int pxValueFromTop) {
		String js = "$('#" + containerId + "').mCustomScrollbar('scrollTo',$('#" + pxValueFromTop + "'));";
		executeJScript(js);
	}

	public void scrollWebElementIntoView(WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public void focusOnElementByCSSWithJS(String elementCSS) {
		String js = String.format("$(\"%s\").focus();", elementCSS);
		executeJScript(js);
	}

	public void multiSelectDropDownByText(LocatorType locType, String locatorValue, List<String> valueToSelect) {
		WebElement el = getElement(locType, locatorValue, DEFAULT_WAIT);
		if (el != null) {
			Select dropdown = new Select(el);
			el.sendKeys(Keys.LEFT_CONTROL);
			for (String value : valueToSelect)
				dropdown.selectByVisibleText(value);
		}

	}

	/**
	 * This function will select the option based on custom attribute like
	 * label,text etc
	 * 
	 * @param type
	 * @param locatorValue
	 * @param wait
	 * @param attribute
	 * @param text
	 */
	public void selectDropDownByAttribute(LocatorType type, String locatorValue, int wait, String attribute,
			String text) {
		Utils.wait(1);
		WebElement el = getElement(type, locatorValue, wait);
		if (el != null) {
			Select dropdown = new Select(el);
			String elemValue = dropdown.getOptions().stream()
					.filter(elem -> elem.getAttribute(attribute).equalsIgnoreCase(text)).findFirst().get()
					.getAttribute("value");
			dropdown.selectByValue(elemValue);
		}
	}

	/**
	 * 
	 * @param element
	 * @param text
	 */
	public void setTextAreaUsingJavaScript(String script, WebElement element, String text) {

		js.executeScript(script, element, text);
	}

	/**
	 * @param element   : Element to check for waiting.
	 * @param timoutSec : time out in sec
	 * @param           pollingSec: polling time
	 * @return : webelement
	 */
	public WebElement waitforElement(WebElement element, int timoutSec, int pollingSec) {

		FluentWait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(timoutSec, TimeUnit.SECONDS)
				.pollingEvery(pollingSec, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, TimeoutException.class)
				.ignoring(StaleElementReferenceException.class);
		try {
			fWait.until(ExpectedConditions.visibilityOf(element));
			/* fWait.until(ExpectedConditions.elementToBeClickable(element)); */
		} catch (Exception e) {
			System.out.println("Page Taking longer time to load....");
			System.out.println("Element Not found trying again - " + element.toString().substring(70));
			e.printStackTrace();
		}
		return element;
	}

	/**
	 * @param element      : Element to check for waiting.
	 * @param timoutSec    : time out in sec
	 * @param              pollingSec: polling time
	 * @param expectedText : Text to be present in the element
	 * @return : boolean
	 */
	public boolean waitforTextChangeOfElement(WebElement element, int timoutSec, int pollingSec, String expectedText) {

		FluentWait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(timoutSec, TimeUnit.SECONDS)
				.pollingEvery(pollingSec, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, TimeoutException.class)
				.ignoring(StaleElementReferenceException.class);
		try {
			fWait.until(ExpectedConditions.textToBePresentInElement(element, expectedText));
			return true;
		} catch (Exception e) {
			System.out.println("Page Taking longer time to load....");
			System.out.println("Element Not found trying again - " + element.toString().substring(70));
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * Java Script execution of check if the checkbox is checked
	 * 
	 * @param CSS of the Element
	 * @return a boolean value based on the checked status
	 */

	public boolean isCheckBoxChked(String Element_CSS) {
		String script = "$(\"" + Element_CSS + "\").is(':checked')";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return (boolean) js.executeScript("return " + script);

	}

	/**
	 * Java Script execution to scroll down to the bottom of the page
	 * 
	 */

	public void scrollBottom() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String command = "$(window.scrollTo(0, document.body.scrollHeight))";
		js.executeScript(command);
	}

	/**
	 * Used to focus on a particular element
	 * 
	 * @param cssSelector
	 */
	public void focusToElementByCSS(String cssSelector) {
		String command = "$(" + cssSelector + ").focus();";
		executeJScript(command);
	}

	/**
	 * Used to focus on a particular element
	 * 
	 * @param id
	 */
	public void focusToElementById(String id) {
		String command = "$('#" + id + "').focus();";
		executeJScript(command);
	}

	/**
	 * Scroll to Particular Element
	 * 
	 * @param element
	 */
	public void scrollTo(WebElement element) {
		js.executeScript("arguments[0].scrollIntoView();", element);
	}

	/**
	 * Retrieves the First selected option in Select Drop Down
	 * 
	 * @param element
	 * @return
	 */
	public String getFirstSelectedOption(WebElement element) {
		Select sel = new Select(element);
		return sel.getFirstSelectedOption().getText();
	}

	/**
	 * Java Script execution of Click
	 * 
	 * @param eleToClick
	 */
	public void jsClick(WebElement eleToClick) {
		js.executeScript("arguments[0].click()", eleToClick);
	}

	/**
	 * copies the file path to clipboard and paste the file path.
	 * 
	 * @param content
	 */
	public void uploadFilePath(String filePath) {
		StringSelection ss = new StringSelection(filePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

		Robot robot;
		try {
			robot = new Robot();
			robot.delay(50);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.delay(500);
			robot.keyPress(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public void waitForPageToLoad() {
		try {
			Utils.wait(3);
			// waitAngularElementToHidden(LocatorType.CLASSNAME, PAGE_MASK_CLASS, 120);
			Utils.wait(1);
		} catch (Exception e) {
			LOG.error("exception while page wait: ", e);
		}
	}

	public void waitUntilClassChanges(LocatorType type, String locator, String classnotcontains) throws Exception {
		By tempBy = null;
		switch (type) {
		case ID:
			tempBy = By.id(locator);
			break;
		case CSSSELECTOR:
			tempBy = By.cssSelector(locator);
			break;
		case CLASSNAME:
			tempBy = By.className(locator);
			break;
		case TAGNAME:
			tempBy = By.tagName(locator);
			break;
		case NAME:
			tempBy = By.name(locator);
			break;
		case XPATH:
			tempBy = By.xpath(locator);
			break;
		default:
			throw new Exception("Unimplemented By Clause");
		}
		final By bylocator = tempBy;
		(new WebDriverWait(driver, DEFAULT_WAIT)).until(new ExpectedCondition<Boolean>() {

			@Override
			public Boolean apply(WebDriver driver) {
				WebElement ele = driver.findElement(bylocator);
				if (null != ele && ele.getAttribute("class").contains(classnotcontains))
					return false;
				return true;
			}

		});
	}

	public void executeMouseOver(WebElement elem) {
		js.executeScript("var x= arguments[0]; $(x).mouseover();", elem);
	}

}
