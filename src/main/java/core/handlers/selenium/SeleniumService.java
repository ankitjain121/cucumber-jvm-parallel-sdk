package core.handlers.selenium;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;

import org.apache.log4j.Logger;

public class SeleniumService {
	private static Logger log = Logger.getLogger(SeleniumService.class);

	public boolean takeScreenShot(WebDriver driver, String outputdir, String fileName) {

		log.debug("capturing screenshot.");

		boolean captured = false;

		try {

			if (driver != null) {

				log.debug("driver is not null.");
				WebDriver augmentedDriver = new Augmenter().augment(driver);

				File screenshot = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
				log.debug("captured screenshot.");

				File ofile = new File(outputdir, fileName);
				log.debug("copying screenshot to dir");

				try {
					FileUtils.copyFile(screenshot, ofile);
				} catch (IOException e) {
					log.debug("capturing screen failed: -", e);
					e.printStackTrace();
					captured = false;
				}

			} else {
				log.error("Could not captured the screen, Driver is Null.");
				captured = false;
			}

		} catch (Exception e) {
			captured = false;
			log.error("Could not captured the screen.", e);
		}

		return captured;
	}

	public void turnOffImplicitWaits(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		log.info("turned OFF driver implicitlyWait");
	}

	public void turnOnImplicitWaits(WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		log.info("turned ON driver implicitlyWait to 30 sec.");
	}

	private Capabilities getFirefoxCapabilities() {
		final FirefoxProfile profile = new FirefoxProfile();
		FirefoxOptions options = new FirefoxOptions().setProfile(profile);
		
		return options.toCapabilities();
	}

	private Capabilities getIECapabilities() {
		FirefoxOptions options = new FirefoxOptions();
		DesiredCapabilities browserCapabilities = DesiredCapabilities.internetExplorer();
		browserCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
				true);
		options.addCapabilities(browserCapabilities);

		return options.toCapabilities();
	}

	private Capabilities getChromeCapabilities() {
		FirefoxOptions options = new FirefoxOptions();
		DesiredCapabilities browserCapabilities = DesiredCapabilities.chrome();
		options.addCapabilities(browserCapabilities);

		return options.toCapabilities();
	}

	public String formSeleniumURL(String seleServerInfo) throws Exception {
		String selhost[] = Pattern.compile(":").split(seleServerInfo);
		if ((selhost.length != 2) || (isEmpty(selhost[0]) && isEmpty(selhost[1]))) {
			throw new Exception(
					"Provided selenium server host is improper. Provide selenium server name in <Host:Port> format");
		}

		return "http://" + seleServerInfo + "/wd/hub";
	}

	public boolean isEmpty(String s) {

		return ((s == null) || (s.length() == 0)) ? true : false;

	}

	public void closeDriver(WebDriver driver) {
		if (driver != null) {
			driver.close();
		} else {
			log.info("found driver null while closing it.");
		}
	}

}
