package core.handlers;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import ch.qos.logback.classic.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class XMLHandler.
 */
public class XMLHandler {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = null;
    
    /** The xml document. */
    private Document xmlDocument = null;
    
    /** The namespace mappings. */
    private LocalNamespaceContext namespaceMappings = new LocalNamespaceContext();

    /**
     * Instantiates a new XML handler.
     *
     * @param absoluteFile the absolute file
     * @throws Exception the exception
     */
    public XMLHandler(File absoluteFile) throws Exception {
        if (absoluteFile.exists()) {
            createDocumentAndCollectPrefixes(new InputSource(new FileReader(absoluteFile)), false);
        } else {
            throw new IOException("File does not exist!");
        }
    }

    /**
     * Instantiates a new XML handler.
     *
     * @param absoluteFile the absolute file
     * @param scanEntireDocumentForNamespaces the scan entire document for namespaces
     * @throws Exception the exception
     */
    public XMLHandler(File absoluteFile, boolean scanEntireDocumentForNamespaces) throws Exception {
        if (absoluteFile.exists()) {
            createDocumentAndCollectPrefixes(new InputSource(new FileReader(absoluteFile)), scanEntireDocumentForNamespaces);
        } else {
            throw new IOException("File does not exist!");
        }
    }

    /**
     * Instantiates a new XML handler.
     *
     * @param sourceXML the source XML
     * @throws Exception the exception
     */
    public XMLHandler(String sourceXML) throws Exception {
        createDocumentAndCollectPrefixes(new InputSource(new StringReader(sourceXML.trim().replaceFirst("^([\\W]+)<", "<"))), false);
    }

    /**
     * Instantiates a new XML handler.
     *
     * @param sourceXML the source XML
     * @param scanEntireDocumentForNamespaces the scan entire document for namespaces
     * @throws Exception the exception
     */
    public XMLHandler(String sourceXML, boolean scanEntireDocumentForNamespaces) throws Exception {
        createDocumentAndCollectPrefixes(new InputSource(new StringReader(sourceXML.trim().replaceFirst("^([\\W]+)<", "<"))), scanEntireDocumentForNamespaces);
    }

    /**
     * Creates the document and collect prefixes.
     *
     * @param documentSource the document source
     * @param scanEntireDocument the scan entire document
     * @throws Exception the exception
     */
    private void createDocumentAndCollectPrefixes(InputSource documentSource, boolean scanEntireDocument) throws Exception {
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true);
        this.xmlDocument = domFactory.newDocumentBuilder().parse(documentSource);
        this.namespaceMappings.findNamespaces(this.xmlDocument.getFirstChild(), scanEntireDocument);
    }

    /**
     * The Class LocalNamespaceContext.
     */
    class LocalNamespaceContext implements NamespaceContext {

        /** The default namespace. */
        private final String defaultNamespace = "";
        
        /** The prefix mapped to uri. */
        private Map<String, String> prefixMappedToUri = new HashMap<String, String>();
        
        /** The uri mapped to prefix. */
        private Map<String, String> uriMappedToPrefix = new HashMap<String, String>();

        /**
         * Find namespaces.
         *
         * @param node the node
         * @param attributesOnly the attributes only
         */
        public void findNamespaces(Node node, boolean attributesOnly) {
            NamedNodeMap attributes = node.getAttributes();
            if (attributes == null) {
                return;
            }
            for (int currentAttribute = 0; currentAttribute < attributes.getLength(); currentAttribute++) {
                storeAttribute((Attr) attributes.item(currentAttribute));
            }
            if (!attributesOnly) {
                NodeList childNodes = node.getChildNodes();
                for (int child = 0; child < childNodes.getLength(); child++) {
                    if (childNodes.item(child).getNodeType() == Node.ELEMENT_NODE) {
                        findNamespaces(childNodes.item(child), false);
                    }
                }
            }
        }

        /**
         * Store attribute.
         *
         * @param attribute the attribute
         */
        private void storeAttribute(Attr attribute) {
            if (attribute.getNamespaceURI() != null && attribute.getNamespaceURI().equals(XMLConstants.XMLNS_ATTRIBUTE_NS_URI)) {
                if (attribute.getNodeName().equals(XMLConstants.XMLNS_ATTRIBUTE)) {
                    addToContext(attribute.getNodeValue());
                } else {
                    addToContext(attribute.getLocalName(), attribute.getNodeValue());
                }
            }
        }

        /**
         * Adds the to context.
         *
         * @param prefix the prefix
         * @param uri the uri
         */
        private void addToContext(String prefix, String uri) {
            this.prefixMappedToUri.put(prefix, uri);
            this.uriMappedToPrefix.put(uri, prefix);
        }

        /**
         * Adds the to context.
         *
         * @param uri the uri
         */
        private void addToContext(String uri) {
            this.prefixMappedToUri.put(this.defaultNamespace, uri);
            this.uriMappedToPrefix.put(uri, this.defaultNamespace);
        }

        /* (non-Javadoc)
         * @see javax.xml.namespace.NamespaceContext#getNamespaceURI(java.lang.String)
         */
        public String getNamespaceURI(String prefix) {
            if (prefix == null || prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
                return this.prefixMappedToUri.get(this.defaultNamespace);
            } else {
                return this.prefixMappedToUri.get(prefix);
            }
        }

        /* (non-Javadoc)
         * @see javax.xml.namespace.NamespaceContext#getPrefix(java.lang.String)
         */
        public String getPrefix(String namespaceURI) {
            return uriMappedToPrefix.get(namespaceURI);
        }

        
        /* (non-Javadoc)
         * @see javax.xml.namespace.NamespaceContext#getPrefixes(java.lang.String)
         */
        public Iterator getPrefixes(String namespaceURI) {
            ArrayList<String> prefixes = new ArrayList<String>();
            for (String URI : this.uriMappedToPrefix.keySet()) {
                if (URI.equals(namespaceURI)) {
                    prefixes.add(URI);
                }
            }
            return prefixes.iterator();
        }
    }

    
    /**
     * Construct an X path expression.
     *
     * @param xPathLocator XPath location to build the Expression from.
     * @return the x path expression - Created Expression.
     * @throws XPathExpressionException the x path expression exception
     */
    private XPathExpression constructAnXPathExpression(String xPathLocator) throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        xPath.setNamespaceContext(namespaceMappings);
        return xPath.compile(xPathLocator);
    }

    /**
     * Create an Element reference.
     *
     * @param locator - XPath location of the element.
     * @return Element - Element object referenced by the XPath locator.
     * @throws XPathExpressionException the x path expression exception
     */
    private Element getElement(String locator) throws XPathExpressionException {
        return (Element) constructAnXPathExpression(locator).evaluate(this.xmlDocument, XPathConstants.NODE);
    }

    /**
     * Return the result of an XPath query on an XML Document in int format.
     *
     * @param query - The XPath query
     * @return int - Result of the query
     * @throws Exception the exception
     */
    public int performXPathQueryReturnInteger(String query) throws Exception {
        return Integer.parseInt(performXPathQueryReturnString(query));
    }

    /**
     * Return the result of an XPath query on an XML Document in String format.
     *
     * @param query - The XPath query
     * @return String - Result of the query
     * @throws Exception the exception
     */
    public String performXPathQueryReturnString(String query) throws Exception {
        return constructAnXPathExpression(query).evaluate(this.xmlDocument, XPathConstants.STRING).toString();
    }

    /**
     * Add text to an element.
     *
     * @param text    - Text to add to the element.
     * @param locator - XPath location of the element.
     * @throws Exception the exception
     */
    public void addTextToElement(String text, String locator) throws Exception {
        Element element = getElement(locator);
        Node textNode = xmlDocument.createTextNode(text);
        element.appendChild(textNode);
    }

    /**
     * Add a child element to an existing element (Will go to the end of the list).
     *
     * @param elementType - Type of child element to add (e.g. div)
     * @param locator     - XPath location of the element.
     * @throws Exception the exception
     */
    public void addChildElement(String elementType, String locator) throws Exception {
        Element element = getElement(locator);
        Node childNode = xmlDocument.createElement(elementType);
        element.appendChild(childNode);
    }

    /**
     * Add an attribute to an element.
     *
     * @param attributeType - Attribute to add (e.g. class).
     * @param value         - Value to assign to the attribute.
     * @param locator       - XPath location of the attribute.
     * @throws Exception the exception
     */
    public void addAttribute(String attributeType, String value, String locator) throws Exception {
        Element element = getElement(locator);
        element.setAttribute(attributeType, value);
    }

    
    /**
     * Return the current XML as a string.
     *
     * @return the string
     * @throws Exception the exception
     */
    public String returnXML() throws Exception {
        StringWriter xmlAsString = new StringWriter();
        TransformerFactory.newInstance().newTransformer().transform(new DOMSource(this.xmlDocument), new StreamResult(xmlAsString));
        return xmlAsString.toString().trim();
    }

   
    
    /**
     * Write XML file.
     *
     * @param absoluteFileName - Absolute filename to write XML object to
     * @return the string - Directory that file has been written to
     * @throws Exception the exception
     */
    public String writeXMLFile(String absoluteFileName) throws Exception {
        if (this.xmlDocument == null) {
            LOGGER.error("The Document object is null, unable to generate a file!");
            return null;
        }
        Source source = new DOMSource(this.xmlDocument);
        FileHandler outputFile = new FileHandler(absoluteFileName, true);
        try {
            Result output = new StreamResult(outputFile.getWriteableFile());
            TransformerFactory.newInstance().newTransformer().transform(source, output);
        } catch (TransformerConfigurationException Ex) {
            LOGGER.error(" Error creating file: " + Ex);
        } catch (TransformerException Ex) {
            LOGGER.error(" Error creating file: " + Ex);
        }
        outputFile.close();
        return outputFile.getAbsoluteFile();
    }
}